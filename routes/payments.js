const express = require('express')

const paymentRouter = express.Router()

const PaymentController = require('../controllers/payments')
const AuthMiddleware = require('../middleware/auth-middleware')

paymentRouter.get('/list', AuthMiddleware.verifyToken, PaymentController.getListPayment)

paymentRouter.get('/sign-amazon-pay', PaymentController.signAmazonPayment)

paymentRouter.get('/checkout-session-amazon-pay', PaymentController.checkoutSessionAmazonPayment)

// paymentRouter.post('/create', AuthMiddleware.verifyToken, PaymentController.createPayment)

paymentRouter.get('/checkout-sesssion/success', PaymentController.checkoutSessionAmazonPaymentSuccess)

paymentRouter.get('/checkout-session/info/:sessionId', PaymentController.checkoutSessionAmazonPaymentInfo)

paymentRouter.post('/checkout-session/update/:sessionId', PaymentController.updateSessionAmazonPayment)

paymentRouter.get('/checkout-session/sent-point/:sessionId', PaymentController.sentPoint)

paymentRouter.post('/checkout-session/complete/:sessionId', PaymentController.checkoutSessionComplete)

paymentRouter.get('/sent-point', PaymentController.sentPoint)

module.exports = paymentRouter