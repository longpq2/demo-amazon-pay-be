const express = require('express')

const userRouter = express.Router()

const UserController = require('../controllers/users')
const AuthMiddleware = require('../middleware/auth-middleware')

userRouter.get('/list', AuthMiddleware.verifyToken, UserController.getListUser)

userRouter.get('/:id', UserController.getUserById)

userRouter.post('/create', UserController.createUser)

userRouter.put('/:id', UserController.updateUser)



module.exports = userRouter