const express = require('express')

const videoRouter = express.Router()

const VideoController = require('../controllers/videos')
const AuthMiddleware = require('../middleware/auth-middleware')

videoRouter.get('/list', AuthMiddleware.verifyToken, VideoController.getAllVideo)

module.exports = videoRouter
