const bcrypt = require('bcryptjs')

class EncryptPassword {
  static encrypt(pass) {
    return bcrypt.hashSync(pass, 8);
  }

  static decrypt(hash) {

  }
}

module.exports = EncryptPassword