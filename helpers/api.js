// import axios from 'axios'
const axios = require('axios')
const { v4: uuidv4 } = require('uuid')

const BASE_URL = process.env.AMAZON_API_URL

const instance = axios.create({
  baseURL: BASE_URL,
  timeout: 20000,
  headers: {
    // 'Authorization': `Bearer ${token}`,
    // "Authorization":"Px2e5oHhQZ88vVhc0DO%2FsShHj8MDDg%3DEXAMPLESIGNATURE",
    // "x-amz-pay-date": "20201012T235046Z",
    "x-amz-pay-idempotency-key": uuidv4()
  }
})

class appApi {
  static onError(e) {
    console.log('======> appApi Error: ', e)
    if (e.response.status === 404) {
      alert(404)
      return
    }
  }

  static get(url, payload) {
    return instance
      .get(`${BASE_URL}/${url}`, payload)
      .catch(error => appApi.onError(error))
  }

  static post(url, payload) {
    return instance
      .post(`${BASE_URL}/${url}`, payload)
      .catch(error => appApi.onError(error))
  }

  static put(url, payload) {
    return instance
      .put(`${BASE_URL}/${url}`, payload)
      .catch(error => appApi.onError(error))
  }

  static delete(url, payload) {
    return instance
      .delete(`${BASE_URL}/${url}`, payload)
      .catch(error => appApi.onError(error))
  }
}

module.exports = appApi