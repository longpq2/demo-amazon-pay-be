import FS from 'fs'
import jwt from 'jsonwebtoken'

const privateKey = FS.readFileSync('private.key')

class JWTHelper {
  static sign(payload, res, next) {
    try {
      const token = jwt.sign(
        payload,
        privateKey,
        { algorithm: 'RS256' },
        (err, token) => {
          if (err) {
            next(err)
          }

          res.json({
            success: true,
            status: 200,
            token,
            exprise: 60000
          })
        }
      )
      return token
    } catch (err) {
      next(err)
    }
  }

  static verify(token, next) {
    try {
      const decode = jwt.verify(token, { algorithm: 'RS256' }, privateKey)
      return decode
    } catch (error) {
      next(error)
    }
  }
}

module.exports = JWTHelper