const Client = require('@amazonpay/amazon-pay-api-sdk-nodejs')
const { v4: uuidv4 } = require('uuid')
const axios = require('axios')

const {
  amazonSignConfigs,
  amazonSessionCheckoutConfigs,
  amazonSessionCheckoutCompleteConfigs,
  captureConfigs,

} = require('../configs/amazon')

const {
  AMAZON_STORE_ID,
  URL_CLIENT_REVIEW,
  URL_CLIENT_RESULT,
  AMAZON_API_URL
} = process.env

// const appApi = require('./api')

class AmazonHelper {
  static checkoutSession() {
    const checkoutSessoion = {}

    return checkoutSessoion
  }

  static createCheckoutSession = async () => {
    try {
      const payload = {
        webCheckoutDetails: {
          checkoutReviewReturnUrl: URL_CLIENT_REVIEW,
          // checkoutResultReturnUrl: 'http://localhost:3000/payments/success',
          // checkoutMode: "ProcessOrder",
        },
        storeId: AMAZON_STORE_ID,
        scopes: ["name", "email", "phoneNumber", "billingAddress"],
        deliverySpecifications: {
          specialRestrictions: ["RestrictPOBoxes"],
          addressRestrictions: {
            type: "Allowed",
            restrictions: {
              US: {
                statesOrRegions: ["WA"],
                zipCodes: ["95050", "93405"]
              },
              GB: {
                zipCodes: ["72046", "72047"]
              },
              IN: {
                statesOrRegions: ["AP"]
              },
              JP: {}
            }
          }
        },
        merchantMetadata: {
            merchantReferenceId: "A1Z9XSQHTO18KK",
            merchantStoreName: null,
            noteToBuyer: null,
            customInformation: null
        },
        chargePermissionType: 'OneTime',
      }
      const headers = {
        'x-amz-pay-idempotency-key': uuidv4().toString().replace(/-/g, '')
      };
      const testPayClient = new Client.WebStoreClient(amazonSessionCheckoutCompleteConfigs)
      const checkoutSession = await testPayClient.createCheckoutSession(payload, headers)

      // console.log('========> checkoutSession: ', checkoutSession)
      return checkoutSession;
    } catch (error) {
      console.log('====> getSessionInfo error: ', error)
    }
  }


  static sign = async () => {

    const payClient = new Client.AmazonPayClient(captureConfigs)

    const checkoutSession = await AmazonHelper.createCheckoutSession()

    // const payload = {
    //   webCheckoutDetails: {
    //     checkoutReviewReturnUrl: URL_CLIENT_REVIEW,
    //     // checkoutResultReturnUrl: 'http://localhost:3000/payments/success?',
    //     // amazonPayRedirectUrl: URL_CLIENT_RESULT,
    //     // checkoutCancelUrl:' http://localhost:3000/payments/cancel',
    //     // checkoutMode: "ProcessOrder"
    //   },
    //   storeId: AMAZON_STORE_ID,
    //   // chargePermissionType: 'OneTime',

    //   // paymentDetails: {
    //   //   paymentIntent: 'AuthorizeWithCapture',
    //   //   canHandlePendingAuthorization: false,
    //   //   chargeAmount: { amount: '1.00', currencyCode: 'USD' },
    //   //   totalOrderAmount: null,
    //   //   softDescriptor: 'Descriptor',
    //   //   presentmentCurrency: 'USD',
    //   //   allowOvercharge: null,
    //   //   extendExpiration: null
    //   // },
    //   //   merchantMetadata: {
    //   //     merchantReferenceId: "A1Z9XSQHTO18KK",
    //   //     merchantStoreName: null,
    //   //     noteToBuyer: null,
    //   //     customInformation: null
    //   // },
    //   //   providerMetadata: {
    //   //     providerReferenceId: '123123123'
    //   //   }
    // }

    const payload = {
      webCheckoutDetails: checkoutSession.data.webCheckoutDetails,
      storeId: checkoutSession.data.storeId
    }

    const signature = payClient.generateButtonSignature(payload)

    // console.log('====> signature: ', signature)
    // AmazonHelper.createCheckoutSessionId()

    // const randomId = Date.now() + (Math.random() * 1000000000)

    return { signature, payload }

  }


  static getSessionInfo = async (amazonCheckoutSessionId) => {
    try {
      const payClient = new Client.WebStoreClient(amazonSessionCheckoutConfigs)
      const headerKey = uuidv4()
      const headers = {
        'x-amz-pay-idempotency-key': headerKey
      }
      const info = await payClient.getCheckoutSession(amazonCheckoutSessionId, headers)

      // console.log('====> getSessionInfo: ', info)

      return info
    } catch (error) {
      console.log('====> getSessionInfo error: ', error)
    }
  }

  static updateSessionPayment = async (sessionId, payload) => {
    try {
      const payClient = new Client.WebStoreClient(captureConfigs)
      const info = await payClient.updateCheckoutSession(sessionId, payload)

      console.log('======> INFOR: ', info)
      return info

    } catch (error) {
      console.log('====> updateSessionPayment error: ', error)
    }
  }

  static checkoutSessionComplete = async (sessionId) => {
    try {
      const headers = null

      const payClient = new Client.WebStoreClient(captureConfigs)

      const chargeAmount = {
        amount: '1.00',
        currencyCode: 'USD'
      }

      const completeCheckoutSession = await payClient.completeCheckoutSession(sessionId, { chargeAmount }, headers)

      console.log('====> completeCheckoutSession: ', completeCheckoutSession)

      

      return completeCheckoutSession
    } catch (error) {
      console.log('===> completeCheckoutSession error: ', error)
    }
  }

  static confirmSessionPayment = async (sessionId, payload) => {
    try {
      console.log('================ confirmSessionPayment start ==========')

      // console.log('====> amazonSessionCheckoutConfigs: ', amazonSessionCheckoutConfigs)
      const payClient = new Client.WebStoreClient(amazonSessionCheckoutCompleteConfigs)

      console.log('==============> payClient: ', payClient)
      // const headerKey =  uuidv4()
      // const headers = {
      //     'x-amz-pay-idempotency-key': headerKey
      // }

      const headers = null

      const completeCheckoutSession = await payClient.completeCheckoutSession(sessionId, { "chargeAmount": payload }, headers)

      console.log('=====> completeCheckoutSession: ', completeCheckoutSession)

      return completeCheckoutSession
    } catch (error) {
      console.log('====> completeCheckoutSession error: ', error)
    }
  }
}

module.exports = AmazonHelper