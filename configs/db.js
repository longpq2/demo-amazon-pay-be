const mysql = require('mysql')

const { DB_HOT_NAME, DB_USER, DB_PASSWORD, DB_NAME } = process.env

const connectDB = mysql.createConnection({
  host: DB_HOT_NAME,
  user: DB_USER,
  password: DB_PASSWORD,
  database: DB_NAME
})

connectDB.connect((err) => {
  if (err) {
    console.log('======== Error connect to database ========')
    return
  }
  return connectDB
})

module.exports = connectDB