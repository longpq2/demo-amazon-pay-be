const fs = require('fs')

const {
  URL_CLIENT_REVIEW,
  AMAZON_STORE_ID,
  AMAZON_PUBLIC_KEY,
  URL_CLIENT_RESULT
  // AMAZON_PRIVATE_KEY
} = process.env

const privateKeyFile = fs.readFileSync("configs/private.pem")

const amazonCheckoutSessionConfigs = {
  "webCheckoutDetails": {
    "checkoutReviewReturnUrl": URL_CLIENT_REVIEW,
    "checkoutResultReturnUrl": URL_CLIENT_RESULT,
    // "amazonPayRedirectUrl": URL_CLIENT_RESULT
  },
  "storeId": AMAZON_STORE_ID,
  "scopes": ["name", "email", "phoneNumber", "billingAddress"],
  "deliverySpecifications": {
    "specialRestrictions": ["RestrictPOBoxes"],
    "addressRestrictions": {
      "type": "Allowed",
      "restrictions": {
        "US": {
          "statesOrRegions": ["WA"],
          "zipCodes": ["95050", "93405"]
        },
        "GB": {
          "zipCodes": ["72046", "72047"]
        },
        "IN": {
          "statesOrRegions": ["AP"]
        },
        "JP": {}
      }
    }
  }
}

const amazonSignConfigs = {
  publicKeyId: AMAZON_PUBLIC_KEY,
  privateKey: privateKeyFile,
  "webCheckoutDetails": {
    "checkoutReviewReturnUrl": URL_CLIENT_REVIEW,
    // "checkoutResultReturnUrl": URL_CLIENT_RESULT,
    // "amazonPayRedirectUrl": URL_CLIENT_RESULT
  },
  "storeId": AMAZON_STORE_ID,
  "scopes": ["name", "email", "phoneNumber", "billingAddress"],
  "deliverySpecifications": {
    "specialRestrictions": ["RestrictPOBoxes"],
    "addressRestrictions": {
      "type": "Allowed",
      "restrictions": {
        "US": {
          "statesOrRegions": ["WA"],
          "zipCodes": ["95050", "93405"]
        },
        "GB": {
          "zipCodes": ["72046", "72047"]
        },
        "IN": {
          "statesOrRegions": ["AP"]
        },
        "JP": {}
      }
    }
  }
}

const amazonSessionCheckoutConfigs = {
  publicKeyId: AMAZON_PUBLIC_KEY,
  privateKey: privateKeyFile,
  "webCheckoutDetails": {
    // "checkoutReviewReturnUrl": null,
    "checkoutResultReturnUrl": URL_CLIENT_RESULT,
    // "amazonPayRedirectUrl": URL_CLIENT_RESULT
    // "checkoutMode": "ProcessOrder"
  },
  paymentDetails: {
    paymentIntent: 'Authorize',
    canHandlePendingAuthorization: false,
    chargeAmount: { amount: '1.00', currencyCode: 'USD' },
    totalOrderAmount: null,
    softDescriptor: 'Descriptor',
    presentmentCurrency: 'USD',
    allowOvercharge: null,
    extendExpiration: null
  },
  region: 'us',
  sandbox: false
}


const updateConfig = {
  publicKeyId: AMAZON_PUBLIC_KEY,
  privateKey: privateKeyFile,
  region: 'us',
  sandbox: false
}

const amazonSessionCheckoutCompleteConfigs = {
  publicKeyId: AMAZON_PUBLIC_KEY,
  privateKey: privateKeyFile,
  webCheckoutDetails: {
    // "checkoutReviewReturnUrl": null,
    checkoutResultReturnUrl: URL_CLIENT_RESULT,
    // "amazonPayRedirectUrl": URL_CLIENT_RESULT
    // "checkoutMode": "ProcessOrder"
  },
  // paymentDetails: {
  //   paymentIntent: 'AuthorizeWithCapture',
  //   canHandlePendingAuthorization: false,
  //   chargeAmount: { amount: '1.00', currencyCode: 'USD' },
  //   // totalOrderAmount: null,
  //   // softDescriptor: 'Descriptor',
  //   // presentmentCurrency: 'USD',
  //   // allowOvercharge: null,
  //   // extendExpiration: null
  // },
  region: 'us',
  sandbox: false
}

const captureConfigs = {
  publicKeyId: AMAZON_PUBLIC_KEY,
  privateKey: privateKeyFile,
  region: 'us',
  sandbox: false
}

module.exports = {
  amazonCheckoutSessionConfigs,
  amazonSignConfigs,
  amazonSessionCheckoutConfigs,
  amazonSessionCheckoutCompleteConfigs,
  captureConfigs,
  updateConfig
}