const jwt = require("jsonwebtoken")
const { TOKEN_KEY } = process.env

class AuthMiddleware {
  static verifyToken(req, res, next) {
    const token = req.body.token || req.query.token || req.headers["x-access-token"] || req.headers.authorization

    if (!token) {
      return res.status(403).send('A token is required for authentication!')
    }

    const arr = token.split(' ')

    // if (arr[0] !== 'Bearer') {
    //   return res.status(403).send('Invalid token!')
    // }

    try {
      const decode = jwt.verify(arr[1], TOKEN_KEY)
      req.user = decode
    } catch(error) {
      return res.status(401).send('Invalid token!')
    }
   
    return next()
  }
}

module.exports = AuthMiddleware