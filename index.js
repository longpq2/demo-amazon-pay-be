require('dotenv').config()
const express = require('express')
const cors = require('cors')

const app = express()
const port = process.env.PORT

const userRouter = require('./routes/users')
const authRoute = require('./routes/auth')
const videoRouter = require('./routes/videos')
const paymentRoute = require('./routes/payments')

app.use(cors())

app.use(express.json())


app.use('/api/user', userRouter)
app.use('/api/auth', authRoute)
app.use('/api/video', videoRouter)
app.use('/api/payment', paymentRoute)
app.use('/payment', paymentRoute)



app.listen(port, () => {
  console.log(`Server start on port ${port}`)
})