
const { ERROR_DATA } = require('../constances')
const UsersModal = require('../models/users')

class UserController {
  static getListUser(req, response, next) {
    try {
      UsersModal.getAllUser((err, rows) => {
        console.log('=====> ROWS: ', rows)
        if (err) {
          return response.json(ERROR_DATA)
        }
        return response.json({
          status: 200,
          success: true,
          result: rows
        })
      })
    } catch(error) {
      next(error)
    }
  }

  static getUserById(req, res, next) {
    try {
      const userId = req.params.id

      UsersModal.getUserById(userId, (err, rows) => {
        if (err) {
          return res.json(ERROR_DATA)
        }

        return res.json({
          success: true,
          status: 200,
          result: rows[0]
        })
      })
    } catch (error) {
      next(error)
    }
  }

  static createUser(req, res, next) {
    try {
      const username = req.body.username
      const password = req.body.password
      const email = req.body.email
      const avatar = req.body.avatar

      UsersModal.createUser({
        username,
        password,
        email,
        avatar
      }, (err, rows) => {
        if (err) {
          return res.json(ERROR_DATA)
        }

        return res.json({
          success: true,
          status: 200
        })
      })
    } catch(error) {
      next(error)
    }
  }

  static updateUser(req, res, next) {
    try {
      const userId = req.params.userId
      const email = req.body.email
      const avatar = req.body.avatar

      UsersModal.updateUser({
        userId,
        email,
        avatar
      }, (err, rows) => {
        if (err) {
          return res.json(ERROR_DATA)
        }

        return res.json({
          success: true,
          status: 200
        })
      })
    } catch(error) {
      next(error)
    }
  }

}

module.exports = UserController