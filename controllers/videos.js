const VideosModel = require('../models/videos')
const { ERROR_DATA } = require('../constances')

class VideoController {
  static getAllVideo(req, res, next) {
    try {
      VideosModel.getAllVideo((err, rows) => {
        console.log('ROWS: ', rows)

        if (err) {
          return res.json(ERROR_DATA)
        }
        
        return res.json({
          status: 200,
          success: true,
          result: rows
        })
      })
    } catch(error) {
      next(error)
    }
  }
}

module.exports = VideoController