
const { ERROR_DATA } = require('../constances')
const PaymentsModal = require('../models/payments')
const AmazonHelper = require('../helpers/amazon')

const path = require("path");

class PaymentController {
  static getListPayment(req, response, next) {
    try {
      PaymentsModal.getListPayment((err, rows) => {
        if (err) {
          return response.json(ERROR_DATA)
        }
        return response.json({
          status: 200,
          success: true,
          result: rows
        })
      })
    } catch (error) {
      next(error)
    }
  }

  static signAmazonPayment = async (req, res, next) => {
    try {
      const sign = await AmazonHelper.sign()
      console.log('====> signAmazonPayment sign: ', sign)
      res.json({
        status: 200,
        success: true,
        result: sign
      })
    } catch (error) {
      console.log('====> signAmazonPayment: ', error)
      next(error)
    }
  }

  static checkoutSessionAmazonPayment(req, res, next) {
    try {
      const sign = AmazonHelper.sign()
      console.log('====> checkoutSessionAmazonPayment sign: ', sign)
      res.json({
        status: 200,
        success: true,
        result: sign
      })
    } catch (error) {
      console.log('====> signAmazonPayment: ', error)
      next(error)
    }
  }

  static getSessionInfo() {
    try {
      AmazonHelper.getSessionInfo()
    } catch (error) {
      console.log('====> error: ', error)
    }
  }

  static checkoutSessionAmazonPaymentSuccess(req, res, next) {
    try {
      const { amazonCheckoutSessionId } = req.query

      const getCheckoutSession = AmazonHelper.getSessionInfo(amazonCheckoutSessionId)

      return res.send('CHECKOUT SUCCESS')
    } catch (error) {
      next(error)
    }
  }

  static checkoutSessionAmazonPaymentInfo = async (req, res, next) => {
    try {
      const { sessionId } = req.params

      const getCheckoutSession = await AmazonHelper.getSessionInfo(sessionId)

      return res.json({
        status: 200,
        success: true,
        result: getCheckoutSession.data
      })
    } catch (error) {
      next(error)
    }
  }

  static updateSessionAmazonPayment = async (req, res, next) => {
    try {
      // console.log('=====> updateSessionAmazonPayment req: ', req)
      const { sessionId } = req.params
      // const { payload } = req.body

      const payload = {
        webCheckoutDetails: {
          checkoutResultReturnUrl: 'http://localhost:3000/payments/success'
        },
        paymentDetails: {
          paymentIntent: 'Confirm',
          // canHandlePendingAuthorization: false,
          chargeAmount: {
            amount: '1.00',
            currencyCode: 'USD'
          }
        },
      };


      const result = await AmazonHelper.updateSessionPayment(sessionId, payload)

      // tao trong db 1 record, sessionID

      console.log('====> updateSessionAmazonPayment result: ', result.data)

      return res.json({
        status: 200,
        success: true,
        result: result.data
      })

      // if (result.data) {
      //   const payload = result.data.paymentDetails.chargeAmount

      //   console.log('==========> payload ==============: ', payload)

      //   const  confirmResult = await AmazonHelper.confirmSessionPayment(sessionId, payload)

      //   // console.log('===> confirmResult: ', confirmResult)

      //   if (!confirmResult) {
      //     return res.json(ERROR_DATA)
      //   }

      //   return res.json({
      //     status: 200,
      //     success: true,
      //     result: confirmResult
      //   })
      // }
    } catch (error) {
      console.log({ error })
      next(error)
    }
  }

  static checkoutSessionComplete = async (req, res, next) => {
    try {
      const { sessionId } = req.params

      const result = await AmazonHelper.checkoutSessionComplete(sessionId)
      console.log({ result })

      // const result2 = await AmazonHelper.getSessionInfo(sessionId);


      return res.json({
        status: 200,
        success: true,
        result: result.data,
        // result2: result2.data,
      })
    } catch (error) {
      next(error)
    }
  }

  static sentPoint(req, res, next) {
    // res.sendFile('views/payments/index.html')
    res.sendFile(path.join(__dirname + '/views/payments/index.html'))
    // res.redirect('http://localhost:3000/payments/success')
  }

  // static createUser(req, res, next) {
  //   try {
  //     const username = req.body.username
  //     const password = req.body.password
  //     const email = req.body.email
  //     const avatar = req.body.avatar

  //     UsersModal.createUser({
  //       username,
  //       password,
  //       email,
  //       avatar
  //     }, (err, rows) => {
  //       if (err) {
  //         return res.json(ERROR_DATA)
  //       }

  //       return res.json({
  //         success: true,
  //         status: 200
  //       })
  //     })
  //   } catch(error) {
  //     next(error)
  //   }
  // }

}

module.exports = PaymentController