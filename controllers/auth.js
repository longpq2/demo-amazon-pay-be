const jwt = require('jsonwebtoken')

const { TOKEN_KEY } = process.env

const { ERROR_DATA } = require('../constances')

const Users = require('../models/users')

class AuthController {
  static login(req, res, next) {
    try {
      const {
        username,
        password
      } = req.body

      console.log('=====> req.body: ', req.body)

      if (!username && !password) {
        return res.status(403).send('Fill all username and password')
      }
  
      Users.getUserByUserNameAndPassword({ username, password }, (error, rows) => {
        console.log('====> rows: ', rows)
        console.log('=====> error: ', error)
        if(error) {
          return res.json(ERROR_DATA)
        }

        // const user = Users.findOne({ username })
        // console.log('====> USER: ', user)

        // if (!user) {
        //   return res.status(403).send('Username or password not correct!')
        // }

        const token = jwt.sign({ username }, TOKEN_KEY, { expiresIn: '7d' })

        return res.json({
          success: true,
          status: 200,
          result: {
            access_token: token,
            refresh_token: '',
            expires: "7d"
          }
        })
      })
    } catch(error) {
      next(error)
    }
  }

  static register(req, res, next) {
    try {
      const {
        username,
        password,
        email,
        avatar
      } = req.body

      if (!username && !password) {
        return res.status(403).send('Username and password is not null!')
      }

      Users.createUser({ username, password, email, avatar}, (error, rows) => {
        if(error) {
          return res.json(ERROR_DATA)
        }

        const user = Users.findOne({ username })

        if (user.username) {
          return res.status(403).send("Username is exits in system!")
        }

        return res.json({
          status: 200,
          success: true
        })
      })
    } catch(error) {
      next(error)
    }
  }
}

module.exports = AuthController