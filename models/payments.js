const connectDB = require('../configs/db')

const PaymentsModel = {
  getListPayment: (cb) => {
    const sql = 'SELECT * FROM payments'

    return connectDB.query(sql, cb)
  },
  // createPayment: ({ points, yenPrices }, cb) => {
  //   const sql = `INSERT INTO payments(user_id, points, yen_prices) VALUES(2, ${points}, ${yenPrices})`

  //   return connectDB.query(sql, cb)
  // }
}

module.exports = PaymentsModel