const connectDB = require('../configs/db')

const EncryptPassword = require('../helpers/encrypt-password')

const UsersModal = {

  getAllUser: (cb) => {
    const query = 'SELECT * FROM users'
    return connectDB.query(query, cb)
  },

  getUserById: (userId, cd) => {
    const query = `SELECT * FROM users WHERE id=${userId}`
    return connectDB.query(query, cd)
  },

  getUserByUserNameAndPassword: ({ username, password }, cb) => {
    const encryptPassword = EncryptPassword.encrypt(password)
    const query = `SELECT * FROM users WHERE username='${username}' AND user_password='${encryptPassword}'`
    
    return connectDB.query(query, cb)
  },

  createUser: ({ username, password, email, avatar = NULL }, cb) => {
    const encryptPassword = EncryptPassword.encrypt(password)
    const query = `INSERT INTO users(username, user_password, email, avatar) VALUES ('${username}', '${encryptPassword}', '${email}', '${avatar}')`
    
    return connectDB.query(query, cb)
  },

  updateUser: ({ userId, email, avatar }, cb) => {
    const query = `UPDATE users SET email=${email} avatar=${avatar} WHERE id=${userId}`

    return connectDB.query(query, cb)
  },

  findOne: ({ username }) => {
    const query = `SELECT * from users WHERE username=${username}`
    return connectDB.query(query)
  }
}

module.exports = UsersModal
