const connectDB = require('../configs/db')

const VideosModel = {
  getAllVideo: (cb) => {
    const query = 'SELECT * FROM videos LIMIT 1'

    return connectDB.query(query, cb)
  }
}

module.exports = VideosModel